{-# LANGUAGE
   ConstraintKinds
 , FlexibleInstances
 , FlexibleContexts
 , GeneralizedNewtypeDeriving
 , MultiParamTypeClasses
 , ScopedTypeVariables
 , TemplateHaskell
 , TypeFamilies
 , TypeOperators
 , TypeSynonymInstances
 , UndecidableInstances
  #-}
--------------------------------------------------------------------------------
-- |
-- Module      :  Graphics.UI.Toy.Button
-- Copyright   :  (c) 2012 Michael Sloan
-- License     :  BSD-style (see the LICENSE file)
-- Maintainer  :  mgsloan@gmail.com
--
-- Simple button UI element.
--
--------------------------------------------------------------------------------
module Graphics.UI.Toy.Button
  ( ButtonState(..), Button(..)

  -- * Lenses
  , buttonState, buttonHit, buttonDiagram

  -- * Mutation
  , clearButtonHit

  -- * Construction
  , mkButton
  ) where

import Control.Lens
import Diagrams.Prelude hiding (view)

import Graphics.UI.Toy
import Graphics.UI.Toy.Diagrams

data ButtonState
  = NormalState
  | HoverState
  | PressState
  deriving (Eq, Ord, Enum, Bounded, Read, Show)

-- | A button stores the state necessary to know if the button is currently
--   being pressed ('_buttonHeld'), and if it was hit ('_buttonHit').  The
--   semantics of '_buttonHit' are up to the user, as it's up to the user to
--   call 'clearButtonHit' or otherwise set its value to @False@.
--
--   In order to draw the button, and figure out when mouse-clicks are inside
--   it, the function '_buttonDiagram' is used. It draws the button based on
--   the current '_buttonHeld' state.
data Button b v n = Button
  { _buttonState   :: ButtonState -- ^ Whether the mouse is hovering / pressing.
  , _buttonHit     :: Bool        -- ^ Whether the button was hit.
  , _buttonDiagram :: Button b v n -> QDiagram b v n Any
                                  -- ^ Draw button based on the state.
  }

type instance N (Button b v n) = n
type instance V (Button b v n) = v

$(makeLenses ''Button)

-- | Builds a button, given the function used to draw it.  The first argument
--   of this function is a boolean that indicates whether it's held.
mkButton :: (Button b v n -> QDiagram b v n Any) -> Button b v n
mkButton = Button NormalState False

-- | This function literally just 'set's 'buttonHit' to 'False'.
clearButtonHit :: Button b v n -> Button b v n
clearButtonHit = set buttonHit False

instance ( MousePos ib ~ v n
         , HasLinearMap v
         , R2 v
         , Num n )
      => Interactive ib (Button b v n) where
  mouse m i b = return $ transition b
   where
    ci = clickInside b (P (mousePos i))
    transition = case (ci, m, b ^. buttonState) of
      (True, Just (True,  0),          _) -> buttonState .~ PressState
      (True, Just (False, 0), PressState) -> (buttonState .~ HoverState)
                                           . (buttonHit .~ True)
      (True,               _, PressState) -> id
      (True,               _,          _) -> buttonState .~ HoverState
      (False,              _,          _) -> buttonState .~ NormalState

instance Diagrammable b v n Any (Button b v n) where
  diagram x = (x ^. buttonDiagram) x

instance (HasLinearMap v, Metric v, Ord n, Floating n)
      => Enveloped (Button b v n) where
  getEnvelope b = getEnvelope (diagram b :: QDiagram b v n Any)

instance (HasLinearMap v)
      => Clickable (Button b v n) where
  clickInside b = clickInside (diagram b :: QDiagram b v n Any)
