{-# LANGUAGE
    ConstraintKinds
  , DeriveDataTypeable
  , FlexibleContexts
  , FlexibleInstances
  , MultiParamTypeClasses
  , StandaloneDeriving
  , ScopedTypeVariables
  , TemplateHaskell
  , TypeFamilies
  , TypeOperators
  , TypeSynonymInstances
  , UndecidableInstances
  #-}
--------------------------------------------------------------------------------
-- |
-- Module      :  Graphics.UI.Toy.Draggable
-- Copyright   :  (c) 2011 Michael Sloan
-- License     :  BSD-style (see the LICENSE file)
-- Maintainer  :  mgsloan@gmail.com
--
-- Utilities for making things that can be clicked and dragged around.
--
--------------------------------------------------------------------------------
module Graphics.UI.Toy.Draggable
  ( Draggable(..)

  -- * Lenses
  , dragState, dragStart, dragOffset, dragContent

  -- * Query
  , isDragging

  -- * Mutation
  , mouseDrag
  , startDrag, updateDrag, endDrag

  -- * Construction
  , mkDraggable, mkHandle
  ) where

import Control.Lens
import Data.Data              ( Data, Typeable )
import Data.Maybe             ( isJust )
import Data.Semigroup         ( Semigroup )

import Diagrams.Prelude
  ( V, N, Point(..), HasLinearMap, Metric, Additive, R2, V2(..), _xy
  , Enveloped(..), Transformable, HasStyle, TrailLike
  , (.-^), (^-^), (^+^), zero, translate, circle
  )

import Graphics.UI.Toy          ( Interactive(..), MousePos, simpleMouse )
import Graphics.UI.Toy.Diagrams ( Clickable(..), Diagrammable(..), blackLined )


-- | Draggable things are translatable, and store state related to dragging.
--   If the '_dragState' has a value, then it indicates that it is currently
--   being dragged, and stores (current mouse pos, initial mouse pos).
--   Subtracting these and adding two '_dragState' gives 'dragOffset', the
--   amount that the '_dragContent' should be offset when drawing.
data Draggable a = Draggable
  { _dragState :: Maybe (V a (N a), V a (N a))
  , _dragStart :: V a (N a)
  , _dragContent :: a
  }

deriving instance (Read a, Read (V a (N a))) => Read (Draggable a)
deriving instance (Show a, Show (V a (N a))) => Show (Draggable a)
deriving instance (Data a, Data (V a (N a))) => Data (Draggable a)
deriving instance Typeable Draggable

type instance N (Draggable a) = N a
type instance V (Draggable a) = V a

$(makeLenses ''Draggable)

instance ( HasLinearMap v, Metric v
         , V a ~ v, N a ~ n
         , Ord n, Floating n
         , Diagrammable b v n q a, Semigroup q )
        => Diagrammable b v n q (Draggable a) where
  diagram d@(Draggable _ _ a)
    = translate (d ^. dragOffset) $ diagram a

instance ( Clickable a, Num (N a), Additive (V a), MousePos ib ~ V a (N a), R2 (V a) )
      => Interactive ib (Draggable a) where
  mouse = simpleMouse mouseDrag

instance ( Clickable a, Num (N a), Additive (V a) )
      => Clickable (Draggable a) where
  clickInside d p = clickInside (d ^. dragContent) $ p .-^ (d ^. dragOffset)

instance ( Enveloped a, HasLinearMap (V a) )
      => Enveloped (Draggable a) where
  getEnvelope d = translate (d ^. dragOffset)
                $ getEnvelope (d ^. dragContent)

-- | Creates dragging state for some object, with an initial offset.
mkDraggable :: V a (N a) -> a -> Draggable a
mkDraggable = Draggable Nothing

-- | Creates a draggable circle of the given radius.
mkHandle :: (TrailLike a, Transformable a, HasStyle a, V a ~ V2, N a ~ Double)
         => Double -> Draggable a
mkHandle = mkDraggable zero . blackLined . circle

-- | Pure mouse handler, compatible with the type expected by "simpleMouse".
--   Only triggers for left mouse clicks (mouse 1).
mouseDrag :: (Eq t, Num t, Num (N a), Additive (V a), Clickable a)
          => Maybe (Bool, t) -> V a (N a) -> Draggable a -> Draggable a
mouseDrag m p d = case m of
  (Just (True,  0))
    | clickInside d (P p) -> startDrag p d
  Nothing                 -> updateDrag p d
  (Just (False, 0))       -> endDrag d
  _                       -> d

-- | Switches into dragging mode at the given position.
startDrag :: V a (N a) -> Draggable a -> Draggable a
startDrag p = dragState .~ Just (p, p)

-- | Updates the drag with a new mouse position, if the object is being dragged.
updateDrag :: V a (N a) -> Draggable a -> Draggable a
updateDrag p (Draggable (Just (_, s)) o c) = Draggable (Just (p, s)) o c
updateDrag _ d = d

-- | Switches out of dragging mode.
endDrag :: (Num (N a), Additive (V a)) => Draggable a -> Draggable a
endDrag d = Draggable Nothing (d ^. dragOffset) (d ^. dragContent)

-- | Queries whether the 'Draggable' is currently being dragged.
isDragging :: Draggable a -> Bool
isDragging = isJust . view dragState

-- | Lens on the current amount of offset for the diagram.
dragOffset :: forall a . (Num (N a), Additive (V a)) => Lens' (Draggable a) (V a (N a))
dragOffset = lens getter setter
 where
  delta = maybe zero (uncurry (^-^))
  getter :: Draggable a -> V a (N a)
  getter    (Draggable c a _) = a ^+^ delta c
  setter :: Draggable a -> V a (N a) -> Draggable a
  setter (Draggable c _ x) a' = Draggable c (a' ^-^ delta c) x
